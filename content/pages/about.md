Title: Sobre AC-DC
Date: 2020-05-28
Modified: 2020-05-28

![Logo de la asociación](https://31lflq.bl.files.1drv.com/y4meRYi9m9Ak8mtYmzsPS8gC95-Fm4xh0Q4J6DVxdMKBlIt80AYx4PDT7AKbfezj1-XC3KQjEqdNNThTkp7ExvHy_tYFrG5MllFhY01azLDC4n0_1tD5N0Dt0ancrzdMLGVtMxRg4dsdyo2ovQ97Vo9YuKbdZk2VhHrL4_A7L5lWmDHQp_A8wO_Jae-yNJj9d8omLzWa7xbUkivU8xnzgDqJA/TFAT-Logo.jpg)

# Beneficios

# Estatuto

[Estatuto](https://1drv.ms/b/s!AlIS1xuGgKAO9geRii56a5FAjLNh)

# Comisión Directiva
Presidente: Gustavo Barbarán

Secretario: Tane Da Souza Correa

Tesorero: Nicolas Roberto Hernández

Vocal 1ro.: María Jimena Lopez Morillo

Vocal 2do.: Marta Patricia Alejandra Mendez

Suplente 1ro.: Jimena Elizabeth Gamboni

Suplente 2 do.: Emilio José Marcelo Criado Sutti

# Socios

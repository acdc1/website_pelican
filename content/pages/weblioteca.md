Title: Weblioteca
Date: 2020-06-01
Authors: Alejandro Kolton, JuanPi Carbajal
Summary: Lista de sitios con material científico interesante

Aquí colecionamos y curamos una lista de recursos científicos muy disfrutables!
Si quieres contribuir con esta lista, por favor agrega un pedido en los [Issues](https://gitlab.com/acdc1/website_pelican/-/issues) de nuestra página web.
No te olvides de proveer un link y una pequeña descripción.

[TOC]

# Videos

## En castellano

### Canales

* La casa de la ciencia

    [https://www.youtube.com/channel/UCE7Fi9yaeQJqI1Pj3d3wUqQ](https://www.youtube.com/channel/UCE7Fi9yaeQJqI1Pj3d3wUqQ)

* Proyecto G

    Canal oficial de "Proyecto G", programa de televisión con fines educativos de Canal Encuentro de Argentina

    [https://www.youtube.com/user/ProyectogTelevision](https://www.youtube.com/user/ProyectogTelevision)

* ¡Mozo! Hay un físico en mi sopa

    Física cuántica - Canal Encuentro por Alberto Rojo

    [https://www.youtube.com/user/encuentro](https://www.youtube.com/user/encuentro)

* Alterados por Pi - Canal Encuentro por Adrian Paenza

    [https://www.youtube.com/user/encuentro](https://www.youtube.com/user/encuentro)

* Date un Vlog 

    ¿Quieres aprender sobre agujeros negros, el universo, la cuántica de forma fácil y sencilla? Pues que sepas que es imposible. Pero lo intentaremos, lo que te aseguro es que te lo pasarás bien mientras ves cómo estalla tu cerebro.

    [https://www.youtube.com/channel/UCQX_MZRCaluNKxkywkLEgfA](https://www.youtube.com/channel/UCQX_MZRCaluNKxkywkLEgfA)
    
* QuantumFracture 

    Ciencia! ¡y con animaciones! El lado más loco (y real) del Universo... cada jueves.

    [https://www.youtube.com/channel/UCbdSYaPD-lr1kW27UJuk8Pw](https://www.youtube.com/channel/UCbdSYaPD-lr1kW27UJuk8Pw)

* FisicaTv 

    [https://www.youtube.com/channel/UC_fElJizcvzXAim2D94njgw](https://www.youtube.com/channel/UC_fElJizcvzXAim2D94njgw)

* Instituto de Fisica Teorica 

    [https://www.youtube.com/channel/UCk195x4zYdMx4LhqEwhcPng](https://www.youtube.com/channel/UCk195x4zYdMx4LhqEwhcPng)

* Minuto de Fisica

    En pocas palabras: física genial y ciencia interesante "Si no puedes explicar algo de forma sencilla, entonces no lo entiendes" ~¿Rutherford vía Einstein? (wikiquote)

    [https://www.youtube.com/channel/UCMnPZh6PyA5PSYoNt0cjuxg](https://www.youtube.com/channel/UCMnPZh6PyA5PSYoNt0cjuxg)

* Minuto de la Tierra

    ¡Ciencia e historias acerca de nuestro increíble planeta!
    Creado por Henry Reich, con Alex Reich, Peter Reich, Emily Elert, Kate Yoshida y Ever Salazar. Música por Nathaniel Schroeder. Traducido por Ever Salazar
    "Cuando tratamos a escoger cualquier cosa por sí misma, encontramos que está enganchada a todo lo demás en el universo." - John Muir

    [https://www.youtube.com/user/MinutoDeLaTierra](https://www.youtube.com/user/MinutoDeLaTierra)

* fq-experimentos

    Experimentos caseros para aprender física y química, biología y matemáticas.  En fq-experimentos encontrarás cientos de experimentos divertidos que puedes hacer en casa o en el colegio con materiales corrientes y muchas ideas para un proyecto científico o una feria de ciencias.

    [https://www.youtube.com/user/fqmanuel](https://www.youtube.com/user/fqmanuel)

* Margarete Heiberg - Museo de Física

    El Museo de Física es un espacio de encuentro con la ciencia abierto a todo el público. En su interior, alberga una colección de más de 2.000 instrumentos utilizados para la enseñanza de la Física en las universidades de principios del siglo XX.
    Debido a que el Museo está diseñado de manera didáctica y pedagógica por un equipo de profesionales de diferentes disciplinas, ofrece una atractiva propuesta para todos aquellos interesados en acercarse al conocimiento de la Física.

    [https://www.youtube.com/user/elmuseodefisica](https://www.youtube.com/user/elmuseodefisica)

* Socratica Español

    Socratica elabora videos educacionales lindos y sencillos en diferentes idiomas. Este canal es el lugar para nuestros videos en Español.
    
    [https://www.youtube.com/user/SocraticaEspanol](https://www.youtube.com/user/SocraticaEspanol)

## En Inglés

### Canales

* 3Blue1Brown 

    “3blue1brown, by Grant Sanderson, is some combination of math and entertainment, depending on your disposition. The goal is for explanations to be driven by animations and for difficult problems to be made simple with changes in perspective” Probablemente el mejor canal de animaciones explicativas sobre los modelos matematicos de distintos fenomenos

    [https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw)

* Veritasium 

    “An element of truth - videos about science, education, and anything else I find interesting”. 

    [https://www.youtube.com/channel/UCHnyfMqiRRG1u-2MsSQLbXA](https://www.youtube.com/channel/UCHnyfMqiRRG1u-2MsSQLbXA)

* Minute Physics

    Simply put: cool physics and other sweet science. "If you can't explain it simply, you don't understand it well enough." ~Rutherford via Einstein? (wikiquote)

    [https://www.youtube.com/user/minutephysics](https://www.youtube.com/user/minutephysics)

* Minute Earth

    Science and stories about our awesome planet!
    Created by Henry Reich, with Alex Reich, Peter Reich, Ever Salazar, Kate Yoshida, David Goldenberg, Arcadi Garcia, Sarah Berman, Jasper Palfree, and Julián Gómez. Music by Nathaniel Schroeder.
    "When we try to pick out anything by itself, we find it hitched to everything else in the Universe." ― John Muir
    
    [https://www.youtube.com/user/minuteearth](https://www.youtube.com/user/minuteearth)

* Vsauce 

    “Descripción Our World is Amazing. Questions? Ideas? Tweet me: http://www.twitter.com/tweetsauce Vsauce was created by Michael Stevens in the summer of 2010. Vsauce is...”

    [https://www.youtube.com/channel/UC6nSFpj9HTCZ5t-N3Rm3-HA](https://www.youtube.com/channel/UC6nSFpj9HTCZ5t-N3Rm3-HA)

* Physics Girl

    Physics Girl is a YouTube channel created by Dianna Cowern that adventures into the physical sciences with experiments, demonstrations, and cool new discoveries. Physics Girl has videos for every atom and eve.
    
    [https://www.youtube.com/user/physicswoman](https://www.youtube.com/user/physicswoman)

* SmarterEveryDay 

    I explore the world using science.  That's pretty much all there is to it.  Watch 2 videos. If you learn something AWESOME, please subscribe if you feel like I earned it

    [https://www.youtube.com/user/destinws2](https://www.youtube.com/user/destinws2)

* Home Science 

    Welcome to Home Science. You’ll find here a lot of amazing experiments, science experiments, chemical reactions and optical illusions

    [https://www.youtube.com/user/maricv84](https://www.youtube.com/user/maricv84)

* Periodic Videos 

    Your ultimate channel for all things chemistry. A video about each element on the periodic table. And we upload new videos every week about science news, interesting molecules and other stuff from the world of chemistry

    [https://www.youtube.com/user/periodicvideos](https://www.youtube.com/user/periodicvideos)

* Numberphile 

    “Videos about numbers - it's that simple. Videos by Brady Haran”

    [https://www.youtube.com/channel/UCoxcjq-8xIDTYp3uz647V5A](https://www.youtube.com/channel/UCoxcjq-8xIDTYp3uz647V5A)

* Computerphile 

    Videos all about computers and computer stuff. Sister channel of Numberphile

    [https://www.youtube.com/user/Computerphile](https://www.youtube.com/user/Computerphile)

* Objectivity

    Objectivity is a series of videos about science treasures. In each video we uncover amazing objects and manuscripts from the archives, tell you their stories, and discuss them with experts.

    [https://www.youtube.com/channel/UCtwKon9qMt5YLVgQt1tvJKg](https://www.youtube.com/channel/UCtwKon9qMt5YLVgQt1tvJKg)
* Kurzgesagt - In a Nutshell 

    Videos explaining things with optimistic nihilism. We are a small team who want to make science look beautiful. Because it is beautiful

    [https://www.youtube.com/user/Kurzgesagt](https://www.youtube.com/user/Kurzgesagt)

* SciShow 

    At SciShow, we're endlessly curious about what makes the universe tick. If you're passionate about quenching your curiosity about... everything, you're in the right place. Join us ever day to learn about the world around us and beyond

    [https://www.youtube.com/user/scishow](https://www.youtube.com/user/scishow)

* Physics Videos by Eugene Khutoryansky 

    Physics YouTube channel made and run by Eugene Khutoryansky. All my videos are narrated by Kira Vincent. Topics covered include physics, mathematics, and engineering

    [https://www.youtube.com/user/EugeneKhutoryansky](https://www.youtube.com/user/EugeneKhutoryansky)

* A capella science 

    Deep science. Sweet harmony.

    [https://www.youtube.com/user/acapellascience](https://www.youtube.com/user/acapellascience)

* The Science Asylum 

    At the Science Asylum, we present anything related to science from the point of view of Nick Lucid... a physicist already driven a little crazy by science

    [https://www.youtube.com/user/TheScienceAsylum](https://www.youtube.com/user/TheScienceAsylum)

* Two minute papers 

    Awesome research for everyone. Two new science videos every week. You'll love it!

    [https://www.youtube.com/user/keeroyz](https://www.youtube.com/user/keeroyz)

* PBS space time 

    Space Time explores the outer reaches of space, the craziness of astrophysics, the possibilities of sci-fi, and anything else you can think of beyond Planet Earth with our astrophysicist host: Matthew O’Dowd.

    [https://www.youtube.com/channel/UC7_gcs09iThXybpVgjHZ_7g](https://www.youtube.com/channel/UC7_gcs09iThXybpVgjHZ_7g)

* PBS Infinite Series 

    Mathematician Tai-Danae Bradley and physicist Gabe Perez-Giz offer ambitious content for viewers that are eager to attain a greater understanding of the world around them. Math is pervasive - a robust yet precise language - and with each episode you’ll begin to see the math that underpins everything in this puzzling, yet fascinating, universe. 

    [https://www.youtube.com/channel/UCs4aHmggTfFrpkPcWSaBN9g](https://www.youtube.com/channel/UCs4aHmggTfFrpkPcWSaBN9g)

* TED-Ed 

    [https://www.youtube.com/user/TEDEducation](https://www.youtube.com/user/TEDEducation)

* Mathloger

    Enter the world of the Mathologer for really accessible explanations of hard and beautiful math(s). 
    In real life the Mathologer is a math(s) professor at Monash University in Melbourne, Australia and goes by the name of Burkard Polster. 
    These days Marty Ross another math(s) professor, great friend and collaborator for over 20 years also plays a huge role behind the scenes, honing the math(s) and the video scripts with Burkard. If you like Mathologer, also check out years worth of free original maths resources on Burkard and Marty's site [http://www.qedcat.com](http://www.qedcat.com).

    [https://www.youtube.com/channel/UC1_uAIS3r8Vu6JjXWvastJg](https://www.youtube.com/channel/UC1_uAIS3r8Vu6JjXWvastJg)


* Sabine Hossenfelder

    Sabine Hossenfelder has a PhD in physics and is presently a Research Fellow at the Frankfurt Institute for Advanced Studies. She is author of the book "Lost in Math: How Beauty Leads Physics Astray" (Basic Books, 2018) and blogs at [backreaction.blogspot.com](backreaction.blogspot.com)

    [https://www.youtube.com/user/peppermint78](https://www.youtube.com/user/peppermint78)


* Tadashi Tokieda on Numberphile

    [https://www.youtube.com/playlist?list=PLt5AfwLFPxWI9eDSJREzp1wvOJsjt23H_](https://www.youtube.com/playlist?list=PLt5AfwLFPxWI9eDSJREzp1wvOJsjt23H_)


* Animalogic

    Examining the nature of the beast. Subscribe for new episodes every other Friday.

    [https://www.youtube.com/channel/UCwg6_F2hDHYrqbNSGjmar4w/](https://www.youtube.com/channel/UCwg6_F2hDHYrqbNSGjmar4w/)


* CGP Grey

    [https://www.youtube.com/user/CGPGrey](https://www.youtube.com/user/CGPGrey)

* Grand Illusions

    We collect unusual toys, and with a collection of over 20,000 (and rising!) that have been bought over the last 30 years, there is a wide variety of amazing things to share with you! 
    We don't do dolls! Our toys might have a fun or unusual mechanism, can be used to amaze or puzzle people, are a bit magical or maybe a bit scientific. Whatever the angle, they have to have something a bit 'special' about them. 
    Nearly all the toys you see here are from Tim's Toy Collection, and are no longer available in the shops. Many were bought 20 or even 30 years ago!

    [https://www.youtube.com/user/henders007](https://www.youtube.com/user/henders007)

* Journey to the Microcosmos

    Take a dive into the tiny, unseen world that surrounds us! With music by Andrew Huang, footage from James Weiss, and narration by Hank Green, we want to take you on a fascinating, reflective journey through the microcosmos.

    [https://www.youtube.com/channel/UCBbnbBWJtwsf0jLGUwX5Q3g](https://www.youtube.com/channel/UCBbnbBWJtwsf0jLGUwX5Q3g)


* Jelle's Marble Runs

    We are the creators of the Marble League (former MarbleLympics), Marbula One, Marble Rally and many other marble racing videos! With superior quality, a dedicated community and diverse content we do our best to take you with us into the wonderful world of marble racing to experience truly unique entertainment!

    [https://www.youtube.com/channel/UCYJdpnjuSWVOLgGT9fIzL0g](https://www.youtube.com/channel/UCYJdpnjuSWVOLgGT9fIzL0g)


* Piled Higher and Deeper (PHD Comics)

    Ponder. Hypothesize. Discover. PHD illustrates and communicates the ideas, stories and personalities of researchers, scientists and scholars worldwide in creative, compelling, funny and truthful ways.

    [https://www.youtube.com/user/phdcomics](https://www.youtube.com/user/phdcomics)

* Quirkology

    Quirky mind stuff from psychologist, author and magician Richard Wiseman.  If you are into psychology, illusions, bets you always win, magic, and the impossible, this is the place to be.

    [https://www.youtube.com/user/Quirkology](https://www.youtube.com/user/Quirkology)

### Charlas

* Tadashi Tokieda - Toy Models

    [https://www.youtube.com/watch?v=vny6FbR5x6A](https://www.youtube.com/watch?v=vny6FbR5x6A&t=503s)

* Richard Feynmann - Fun to imagine

    [https://www.youtube.com/watch?v=P1ww1IXRfTA](https://www.youtube.com/watch?v=P1ww1IXRfTA)

* Richard Feynman - The World from another point of view

    [https://www.youtube.com/watch?v=GNhlNSLQAFE](https://www.youtube.com/watch?v=GNhlNSLQAFE)

* Richard Feynman - Quantum Mechanical View of Reality 1-4

    [https://www.youtube.com/watch?v=ZcpwnozMh2U](https://www.youtube.com/watch?v=ZcpwnozMh2U)

    [https://www.youtube.com/watch?v=xNF_3KdpdrY](https://www.youtube.com/watch?v=xNF_3KdpdrY)

    [https://www.youtube.com/watch?v=EmXRQDQQeJ4](https://www.youtube.com/watch?v=EmXRQDQQeJ4)

    [https://www.youtube.com/watch?v=MksgbUylt8o](https://www.youtube.com/watch?v=MksgbUylt8o)

* Richard Feynman - The reason for antiparticles

    [https://www.youtube.com/watch?v=MDZaM-Bi-kI](https://www.youtube.com/watch?v=MDZaM-Bi-kI)

* Richard Feynman - on Quantum Mechanics Part 1: Photons Corpuscles of Light


    [https://www.youtube.com/watch?v=xdZMXWmlp9g](https://www.youtube.com/watch?v=xdZMXWmlp9g)

* Santa Fe Institute

    Our researchers endeavor to understand and unify the underlying, shared patterns in complex physical, biological, social, cultural, technological, and even possible astrobiological worlds. Our global research network of scholars spans borders, departments, and disciplines, unifying curious minds steeped in rigorous logical, mathematical, and computational reasoning. As we reveal the unseen mechanisms and processes that shape these evolving worlds, we seek to use this understanding to promote the well-being of humankind and of life on earth.

    [https://www.youtube.com/user/santafeinst](https://www.youtube.com/user/santafeinst)

* Socratica

    Socratica makes high-quality educational videos on math and science.
    
    We're a couple of Caltech grads who believe you deserve better videos. You'll learn more with us! If you like what we do, please share our videos with everyone you know!  This really helps us grow.   :)

    [https://www.youtube.com/user/SocraticaStudios](https://www.youtube.com/user/SocraticaStudios)


### Cursos

* Physics lectures by Walter Lewin

    They will make you ♥ Physics 

    [https://www.youtube.com/channel/UCiEHVhv0SBMpP75JbzJShqw](https://www.youtube.com/channel/UCiEHVhv0SBMpP75JbzJShqw)

* Discrete differential geometry

    Lecture videos for an intro course on Discrete Differential Geometry at Carnegie Mellon University 

    [https://www.youtube.com/playlist?list=PL9_jI1bdZmz0hIrNCMQW1YmZysAiIYSSS](https://www.youtube.com/playlist?list=PL9_jI1bdZmz0hIrNCMQW1YmZysAiIYSSS)

* MIT OpenCourseWare

    Whether you’re a student, a teacher, or simply a curious person that wants to learn, MIT OpenCourseWare (OCW) offers a wealth of insight and inspiration. There's videos, and a whole lot more!
    OCW is a free and open online publication of material from thousands of MIT courses, covering the entire MIT curriculum, ranging from the introductory to the most advanced graduate courses. At the OCW website, you'll find that each course has a syllabus, instructional material like notes and reading lists, and learning activities like assignments and solutions. Some courses also have videos, online textbooks, or faculty insights on teaching.
    Knowledge is your reward. There's no signup or enrollment, and no start/end dates. it's self-paced learning at its best.

    [https://www.youtube.com/user/MIT](https://www.youtube.com/user/MIT)


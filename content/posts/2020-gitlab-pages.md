Title: ACDC usando Pelican en GitLab Pages!
Date: 2020-05-28
Category: Blog
Tags: pelican, gitlab
Slug: tfat-en-gitlab-pages

Este sitio lo hosteamos en GitLab Pages!

El código del sitio lo encontrás en <https://gitlab.com/acdc1/website_pelican>.

Aprene más sobre GitLab Pages en <https://pages.gitlab.io>.

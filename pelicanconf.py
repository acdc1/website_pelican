#!/usr/bin/env python

LOAD_CONTENT_CACHE = False

#THEME= '/home/juanpi/Resources/pelican-themes/attila'
#THEME= '/home/juanpi/Resources/pelican-themes/bootstrap2'
#THEME= '/home/juanpi/Resources/pelican-themes/materialistic'
#THEME= '/home/juanpi/Resources/pelican-themes/bootstrap2'
#THEME= '/home/juanpi/Resources/pelican-themes/Flex'
THEME= 'themes/Flex'


AUTHOR = 'ACDC team'
SITENAME = 'Asociación Civil Ciencia al Alcance de Todos'
SITEURL = ''

SITELOGO = "https://31lflq.bl.files.1drv.com/y4meRYi9m9Ak8mtYmzsPS8gC95-Fm4xh0Q4J6DVxdMKBlIt80AYx4PDT7AKbfezj1-XC3KQjEqdNNThTkp7ExvHy_tYFrG5MllFhY01azLDC4n0_1tD5N0Dt0ancrzdMLGVtMxRg4dsdyo2ovQ97Vo9YuKbdZk2VhHrL4_A7L5lWmDHQp_A8wO_Jae-yNJj9d8omLzWa7xbUkivU8xnzgDqJA/TFAT-Logo.jpg"

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Argentina/Salta'

# Default theme language.
I18N_TEMPLATES_LANG = "en"
# Your language.
DEFAULT_LANG = "es"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Content licensing: CC-BY-SA
CC_LICENSE = {
    "name": "Creative Commons Attribution-ShareAlike",
    "version": "4.0",
    "slug": "by-sa"
}
COPYRIGHT_YEAR = 2020
COPYRIGHT_NAME = "Asociación Ciencia al Alcance de Todos"

MAIN_MENU = True
# Do not show categories on menu
DISPLAY_CATEGORIES_ON_MENU = False
# Do not how pages on menu
DISPLAY_PAGES_ON_MENU = False
# Add manually
MENUITEMS = [
    ['Acciones', '/pages/acciones.html'],
    ['Contacto', '/pages/contacto.html'],
    ['Sobre AC-DC', '/pages/sobre-ac-dc.html'],
  ]

# Links
DISPLAY_PAGES_ON_LINKS = False
LINKS = [
        ['Noticias', '/blog_index.html'],
        ['Weblioteca', '/pages/weblioteca.html'],
        ['Galería', '/pages/galeria.html'],
        ['Taller de Física', 'https://acdc1.gitlab.io/tfat/website_pelican'],
        ]

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

INDEX_SAVE_AS = 'blog_index.html'
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Plugins
# Path to Plugins
PLUGIN_PATHS = ['plugins']
# Enable i18n plugin, probably you already have some others here.
PLUGINS = ['i18n_subsites']
# Enable Jinja2 i18n extension used to parse translations.
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

MARKDOWN = {
  'extension_configs': {
    'markdown.extensions.toc': {
      'title': 'Contenido:' 
    },
    'markdown.extensions.codehilite': {'css_class': 'highlight'},
    'markdown.extensions.extra': {},
    'markdown.extensions.meta': {},
  },
  'output_format': 'html5',
  'extensions' : ['toc']
}

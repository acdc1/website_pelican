#!/usr/bin/env python

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://acdc1.gitlab.io/website_pelican'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Update LINKS and MENITEMS
for  i in range(len(LINKS)):
    if '://' not in LINKS[i][1]:
        LINKS[i][1] = SITEURL + LINKS[i][1]
for i in range(len(MENUITEMS)):
    if '://' not in MENUITEMS[i][1]:
        MENUITEMS[i][1] = SITEURL + MENUITEMS[i][1]

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
